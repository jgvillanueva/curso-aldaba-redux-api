class DataService {
    async getAll() {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes';
        const response = await fetch(url, {
            method: 'GET',
        })
        return response;
    }

    async sendMessage(mensaje) {
        const url = 'http://dev.contanimacion.com/api_tablon/api/mensajes/add';
        console.log(mensaje);
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(mensaje),
            headers: {"Content-type": "application/json"}
        })
        return response;
    }
}
export default new DataService();