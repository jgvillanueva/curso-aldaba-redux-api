
import './App.css';
import TodoList from "./Components/TodoList";
import NewTodo from "./Components/NewTodo";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>ToDos</h2>
      </header>
        <NewTodo />
        <TodoList />
    </div>
  );
}

export default App;
